
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <assert.h>
#include <stdio.h>

#include "renderer.hh"

float verts[] = {
  -0.5f, 0.5f,
  -0.5f, -0.5f,
  0.5f, 0.5f
};

const char* source_shader_vert = "#version 130\n"
  "in vec2 aPos;\n"
  "void main() {\n"
  "  gl_Position = vec4(aPos.x, aPos.y, 1.0f, 1.0f);\n"
  "}\n\0";

const char* source_shader_frag = "#version 130\n"
  "out vec4 fragColor;\n"
  "void main() {\n"
  "  fragColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);\n"
  "}\n\0";

class Renderer::Impl
{
public:
  Impl(void);
  ~Impl(void);

  void init_shaders(void);
  GLuint create_shader(GLenum shader_type, const char* source);
  
  SDL_Window* window;
  SDL_GLContext context;

  bool is_should_close;
  GLuint VBO, VAO, EBO, shader_vert, shader_frag, shader_prog;
};

Renderer::Impl::Impl(void)
  : is_should_close(false)
{
  
  assert(SDL_Init(SDL_INIT_EVERYTHING) == 0x0);

  window = SDL_CreateWindow("test",
			    SDL_WINDOWPOS_UNDEFINED,
			    SDL_WINDOWPOS_UNDEFINED,
			    0x400, 0x300,
			    SDL_WINDOW_OPENGL);

  context = SDL_GL_CreateContext(window);

  SDL_GL_MakeCurrent(window, context);

  glewExperimental = true;
  assert(glewInit() == GLEW_OK);

  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  
  glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), nullptr);
  glEnableVertexAttribArray(0);

  init_shaders();
  return;
}

GLuint
Renderer::Impl::create_shader(GLenum shader_type, const char* source)
{
  GLuint shader;
  shader = glCreateShader(shader_type);
  glShaderSource(shader, 1, &source, NULL);
  glCompileShader(shader);

  int status;
  char compile_log[0x200];
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

  if (status != GL_TRUE)
    {
      glGetShaderInfoLog(shader, 0x200, NULL, compile_log);
      fprintf(stdout, "error, can't compile shader: %s\n", compile_log);
      exit(-0x1);
    }
  
  return shader;
}

void
Renderer::Impl::init_shaders(void)
{
  shader_vert = create_shader(GL_VERTEX_SHADER, source_shader_vert);
  shader_frag = create_shader(GL_FRAGMENT_SHADER, source_shader_frag);
  shader_prog = glCreateProgram();
  glAttachShader(shader_prog, shader_vert);
  glAttachShader(shader_prog, shader_frag);
  glLinkProgram(shader_prog);
  glDeleteShader(shader_vert);
  glDeleteShader(shader_frag);

  int success;
  char compile_log[0x200];
  glGetProgramiv(shader_prog, GL_LINK_STATUS, &success);
  if (!success)
    {
      glGetProgramInfoLog(shader_prog, sizeof(compile_log), NULL, compile_log);
      fprintf(stdout, "error, can't link shaders: %s\n", compile_log);
    }
  return;
}

void
Renderer::test_triangle(void)
{
  glUseProgram(impl->shader_prog);
  glBindVertexArray(impl->VAO);
  glDrawArrays(GL_TRIANGLES, 0, 3);
  return;
}

Renderer::Impl::~Impl(void)
{
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return;
}

Renderer::Renderer(void)
  : impl(std::make_unique<Renderer::Impl>()) {}

Renderer::~Renderer(void) {}

bool
Renderer::should_close(void)
{
  return impl->is_should_close;
}

void
Renderer::clear(void)
{
  make_current();
  glClearColor(0.0f, 0.2f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);
}

void
Renderer::make_current(void)
{
  SDL_GL_MakeCurrent(impl->window, impl->context);
  return;
}

void
Renderer::present(void)
{
  make_current();
  SDL_GL_SwapWindow(impl->window);
  return;
}

void
Renderer::poll_events(void)
{
  SDL_Event event;
  while (SDL_PollEvent(&event) != 0x0)
    {
      switch (event.type)
	{
	case SDL_QUIT:
	  impl->is_should_close = true;
	  break;
	}
    }
  return;
}
