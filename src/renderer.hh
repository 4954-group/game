
#ifndef C4LL_RENDERER_INC
#define C4LL_RENDERER_INC

#include <memory>

class Renderer
{
public:
  Renderer(void);
  ~Renderer(void);

  bool should_close(void);
  void poll_events(void);
  void clear(void);
  void present(void);
  void make_current(void);

  void test_triangle(void);
private:
  class Impl;
  std::unique_ptr<Impl> impl;
};

#endif
