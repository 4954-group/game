
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>

#include <memory>
#include <thread>
#include <chrono>
#include <mutex>
#include <assert.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "renderer.hh"


int
main(void)
{
  std::shared_ptr<Renderer> renderer;

  renderer = std::make_shared<Renderer>();

  while (!renderer->should_close())
    {
      renderer->clear();

      renderer->test_triangle();

      renderer->present();
      renderer->poll_events();
    }

  return 0x0;
}
